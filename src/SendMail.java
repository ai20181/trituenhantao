package AI20181;

import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
*
* @author Tran Huy
*/
public class SendMail {

    public static void send(String smtpServer, String to, String from, String psw,
            String subject, String body) throws Exception {
// Code provide by congdongjava.com
        Properties props = System.getProperties();
        props.put("mail.smtp.host", smtpServer);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.starttls.enable", "true");
        final String login = from;
        final String pwd = psw;
        Authenticator pa = null;
        if (login != null && pwd != null) {
            props.put("mail.smtp.auth", "true");
            pa = new Authenticator() {

                public PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(login, pwd);
                }
            };
        }//else: no authentication
        Session session = Session.getInstance(props, pa);
// â€” Create a new message â€“
        Message msg = new MimeMessage(session);
// â€” Set the FROM and TO fields â€“
        msg.setFrom(new InternetAddress(from));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(
                to, false));

// â€” Set the subject and body text â€“
        msg.setContent(body, "text/html; charset=UTF-8");
        String newSubject = new String(subject.getBytes("UTF-8"), "UTF-8");
        msg.setSubject(newSubject);
        String str_body = new String(body.getBytes("UTF-8"), "UTF-8");
        msg.setText(str_body);
        //Ä�á»ƒ gá»Ÿi ná»™i dung dáº¡ng utf-8 cÃ¡c báº¡n dÃ¹ng msg.setContent(body, "text/html; charset=UTF-8");
// â€” Set some other header information â€“
        msg.setHeader("X-Mailer", "LOTONtechEmail");
        msg.setSentDate(new Date());
        msg.saveChanges();
// â€” Send the message â€“
        Transport.send(msg);
    }
    
  
}